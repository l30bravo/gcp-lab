# Associate Cloud Engineer

## Examen ¿Que cubre?
* Configurar un entorno de Cloud
* Planear y configurar una solucion de Cloud
* Deployar e implementar una solucion de Cloud
* Asegurar la operacion correcta de una solucion de Cloud
* Configurar el acceso y la seguridad.

# gcp-lab 

script, documents, among other things, to learn about GCP

# Links
* google.qwiklabs.com
* https://cloud.google.com/docs/overview
* https://cloud.google.com/products
* https://cloud.google.com/docs/tutorials
* https://cloud.google.com/training
* https://cloud.google.com/free and https://cloud.google.com/free/docs/gcp-free-tier
* https://www.youtube.com/user/googlecloudplatform and https://www.youtube.com/playlist?list=PLIivdWyY5sqL8biXl2L_axB8TpROQMkbQ
* Test: https://cloud.google.com/certification/cloud-engineer
* Guia para el examen: https://cloud.google.com/certification/guides/cloud-engineer/
* Examen de practica: https://cloud.google.com/certification/practice-exam/cloud-engineer
* Doc oficial: https://cloud.google.com/docs/


# ¿Como empiezo con Google Cloud?
* https://www.youtube.com/watch?v=OiDWqu0oQfo

# Crear un Proyecto
* https://cloud.google.com/resource-manager/docs/creating-managing-projects
