# Chapter 3 - Projects, Service Account and Billing

## How to organizes Projects and Accounts

GCP provides a way to group resources and and manage them as a single unit.
This is called resource hierarchy

### Resource Hierachy

The central abstraction for managing GCP resources is the "Resource hierarchy"
it consist in tree levels

1 - Organization
2 - Folders
3 - Projects

#### Organization

Is the root of the resource hierarchy and correspond to a company
this is a G-Suit domain or a Cloud Identity.

A single cloud identity is associated with at most one organization.

Cloud identity have "Super Admins" thouse assing the roles:
- Organization Admin
- Access Manager

In addition automatically grant "Proyects Creator" and billing account creation IAM roles
to all user in the domain. This allow any user to create projects and enable billings.

The Organization Administrator roles are reponsable for:
- Defining the structure of the resource hierarchy
- Defining identity access manager policies over the resource hierarchy.
- Delegating other managment roles to other users.

#### Folders
Folder can cotain other folder or projects.
Folder usually built arround the kind of service provided by the resources
Ussually folder are using from departments and intenal sub-division (sub-folders)

#### Peojects
are in some way the most importan part of the hierarchy.
its in the project tha we create resources.

Your organization will have a quota  of project it can create.
The quota can vary betwenn organizations. (google make a desition based in the consumer's usage history)

You cant request increase this quota, just in case.

#### Organization Policies



