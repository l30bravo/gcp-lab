# GCLOUD

* https://cloud.google.com/sdk/gcloud/reference


# Configure Proxy

* `PROXY_TYPE=` http or https 
*

```
gcloud config set proxy/type $PROXY_TYPE
gcloud config set proxy/address $PROXY_IP_ADDRESS
gcloud config set proxy/port $PROXY_PORT
gcloud config set proxy/username $USERNAME
gcloud config set proxy/password $PASSWORD
```
# Start
```
gcloud init
```

#  Status of the Envirorment
```
gcloud info --run-diagnostics
```

# Login
```
gcloud auth login --no-launch-browser
gcloud config set project $PROYECT_ID
```

## Set Credentials
* get a *.key
* create a directory in  ~/.gcp/
* move the file *.key to  ~/gcp/
* change de *.key to *.json
* export the credential `export GOOGLE_APPLICATION_CREDENTIALS=~/.gcp/file.json`
* `gcloud config set core/account tf-....@xxxxx.gserviceaccount.com`, replace with the corresponding gserviceaccount
* testing with: `gcloud list`

# Cheat Sheet
* Nachine-types list
